package com.rgtc.amazingapp;

// Import necessary Android classes and libraries
import android.Manifest;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

// MainActivity class declaration, extending AppCompatActivity
public class MainActivity extends AppCompatActivity {

    // Constant for identifying internet permission request
    private static final int INTERNET_PERMISSION_REQUEST_CODE = 1;

    // Declare WebView and FloatingActionButton
    private WebView webView;
    private FloatingActionButton fab;

    // Ignore lint warnings related to setting JavaScript enabled
    @SuppressLint("SetJavaScriptEnabled")
    // onCreate method, called when the activity is created
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set content view to the layout defined in "activity_main.xml"
        setContentView(R.layout.activity_main);

        // Initialize WebView and FloatingActionButton
        webView = findViewById(R.id.webView);
        fab = findViewById(R.id.fab);

        // Check and request internet permission if not granted
        if (checkInternetPermission()) {
            setupWebView(); // Initialize WebView if permission granted
        } else {
            requestInternetPermission(); // Request internet permission if not granted
        }

        // Set click listener for FloatingActionButton
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toggle the visibility of the WebView
                toggleWebViewVisibility();

                // Load a new URL into the WebView
                loadNewUrl("https://landbot.online/v3/H-1784607-JGCMXNVKG7T3JHOE/index.html");
            }
        });
    }

    // Method to set up WebView with necessary settings
    private void setupWebView() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
    }

    // Method to load a new URL into the WebView
    private void loadNewUrl(String url) {
        webView.loadUrl(url);
    }

    // Method to check if the app has internet permission
    private boolean checkInternetPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                == PackageManager.PERMISSION_GRANTED;
    }

    // Method to request internet permission
    private void requestInternetPermission() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.INTERNET},
                INTERNET_PERMISSION_REQUEST_CODE
        );
    }

    // Method called when the user responds to a permission request
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Check if the request code matches the internet permission request code
        if (requestCode == INTERNET_PERMISSION_REQUEST_CODE) {
            // Check if the internet permission was granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setupWebView(); // Initialize WebView if permission granted
            } else {
                // Handle permission denied
            }
        }
    }

    // Method to toggle the visibility of the WebView
    private void toggleWebViewVisibility() {
        // If the WebView is visible, hide it; if hidden, show and initialize it
        if (webView.getVisibility() == View.VISIBLE) {
            webView.setVisibility(View.GONE);
        } else {
            setupWebView(); // Initialize WebView if not done already
            webView.setVisibility(View.VISIBLE);
        }
    }
}
